const fs = require('fs');
const PROJECT_VARS = require('./variables');
const path = require('path');
const webpack = require('webpack');
const paths = require('./paths');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const resolveApp = relativePath => path.resolve(appDirectory, relativePath);
const appDirectory = fs.realpathSync(process.cwd());

module.exports = {
  entry: {
    [PROJECT_VARS.PROJECT_ID]: ['./config/polyfills.js', paths.indexJs]
  },
  output: {
    filename: `arquivos/original.js`,
    path: paths.dist,
    publicPath: '/'
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /(node_modules|bower_components)/,
        loader: "babel-loader"
      },
      {
				test: /\.scss$/,
				use: [
          {
						loader: 'file-loader',
						options: {
							name: `/arquivos/original.css`,
						}
					},
          {
            loader: 'postcss-loader',
            options: {
              ident: 'postcss',
              plugins: () => [
                require('postcss-flexbugs-fixes'),
                require('css-mqpacker')({ sort: true }),
                require('autoprefixer')({
                  flexbox: 'no-2009',
                }),
              ],
            },
          },
					{
						loader: 'sass-loader'
					}
				]
			},
      {
        test: /\.svg/,
        loader: 'svg-inline-loader'
      }
    ]
  }
};
