const bs = require("browser-sync").create(),
      path = require('path'),
      { exec } = require('child_process'),
      fs = require('fs');

let buildStatusFile = `${process.env['HOME']}/build-status`;

let build;
if(process.argv[2] === 'checkout') {
  build = 'npm run build:checkout';
} else if(process.argv[2] === 'requestly'){
  build = 'npm run build:requestly';
} else if(process.argv[2] === 'original'){
  build = 'npm run build:original';
} else {
  build = 'npm run build:scss';
}

const rootDir = './build';

fs.writeFileSync(buildStatusFile, 'building...', 'utf8');
exec(build, (error, stdout, stderr) => {
  if(error) {
    console.error('exec error: ', error);
    return;
  }
  console.log('stdout: ', stdout);
  console.log('stderr: ', stderr);
  fs.writeFileSync(buildStatusFile, '', 'utf8');
}).stdout.on('data', function(data) {
  console.log(data);
});

bs.init({
  port: 5000,
  https: true,
  open: false,
  server: {
    baseDir: "./dist"
  }
});

bs.watch('./src/**').on('change', () => {
  fs.writeFileSync(buildStatusFile, 'building...', 'utf8');
  exec(build, (error, stdout, stderr) => {
    if(error) {
      console.error('exec error: ', error);
      return;
    }
    console.log('stdout: ', stdout);
    console.log('stderr: ', stderr);
    fs.writeFileSync(buildStatusFile, '', 'utf8');

    bs.reload();
  }).stdout.on('data', function(data) {
    console.log(data);
  });
});
